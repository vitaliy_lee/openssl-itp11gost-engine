#![allow(clippy::needless_return)]

use std::fmt::{self, Write};
use std::path::{Path, PathBuf};

use openssl::error::ErrorStack;
use openssl::nid::{self, Nid};
use openssl::pkey::{Id, PKey, PKeyCtx, Private, Public};

const ENGINE_ID: &str = "itp11gost";

const SET_LOG_LEVEL_CMD: &str = "ITP11_SET_LOG_LEVEL";
const FINAL_CMD: &str = "ITP11_FINAL";

pub const CKF_LIBRARY_CANT_CREATE_OS_THREADS: i32 = 1;
pub const CKF_OS_LOCKING_OK: i32 = 2;

const ITP11_INIT_PKCS11: &str = "ITP11_INIT_PKCS11";
const ITP11_EVP_PKEY_CTRL_STRING_TOKEN_URI: &str = "token";
const ITP11_EVP_PKEY_CTRL_STRING_CKA_LABEL: &str = "cka_label";
const ITP11_EVP_PKEY_CTRL_STRING_CKA_TOKEN: &str = "cka_token";
const ITP11_EVP_PKEY_CTRL_STRING_CKA_EXTRACTABLE: &str = "cka_extractable";
const ITP11_EVP_PKEY_CTRL_STRING_CKA_SENSITIVE: &str = "cka_sensitive";

const EVP_PKEY_ALG_CTRL: i32 = 0x1000;

const ITP11_EVP_PKEY_CTRL_SET_GOST_PARAMSET: i32 = EVP_PKEY_ALG_CTRL + 1;
// const ITP11_EVP_PKEY_CTRL_GET_GOST_PARAMSET: i32 = EVP_PKEY_ALG_CTRL + 2;
// const ITP11_EVP_PKEY_CTRL_SET_GEN_ATTRS: i32 = EVP_PKEY_ALG_CTRL + 3;
// const ITP11_EVP_PKEY_CTRL_GET_GEN_ATTRS: i32 = EVP_PKEY_ALG_CTRL + 4;
// const ITP11_EVP_PKEY_CTRL_GET_PUB_ATTRS: i32 = EVP_PKEY_ALG_CTRL + 5;
// const ITP11_EVP_PKEY_CTRL_SET_ENCODE_URI: i32 = EVP_PKEY_ALG_CTRL + 14;
// const ITP11_EVP_PKEY_CTRL_SET_KDF: i32 = EVP_PKEY_ALG_CTRL + 15;
// const ITP11_EVP_PKEY_CTRL_SET_MAKE_COPY: i32 = EVP_PKEY_ALG_CTRL + 20;


#[cfg(any(target_os = "macos", target_os = "ios"))]
const DEFAULT_MODULE_PATH: &str = "/Library/Frameworks/soft_token.framework/soft_token";

#[cfg(any(target_os = "linux", target_os = "android"))]
const DEFAULT_MODULE_PATH: &str = "/opt/itcs/lib/libsofttoken_pkcs11.so";

const DEFAULT_MODULE_FLAGS: i32 = CKF_LIBRARY_CANT_CREATE_OS_THREADS | CKF_OS_LOCKING_OK;
const DEFAULT_SESSION_CACHE_SIZE: usize = 16;
const DEFAULT_LOG_LEVEL: LogLevel = LogLevel::Warning;

pub type Result<T> = std::result::Result<T, ErrorStack>;

#[derive(Debug)]
pub struct Token {
    pin: String,
    slot: usize,
    set_default: bool,
}

impl Token {
    pub fn new(slot: usize, pin: String) -> Token {
        let token = Token {
            set_default: false,
            slot,
            pin,
        };

        return token;
    }

    pub fn set_default(&mut self, yes: bool) {
        self.set_default = yes;
    }
}

#[derive(Debug)]
pub struct Module {
    name: String,
    path: PathBuf,
    flags: i32,
    session_cache_size: usize,
    tokens: Vec<Token>,
    initialize: bool,
}

impl Module {
    pub fn new(name: String, tokens: Vec<Token>) -> Module {
        if name.is_empty() {
            panic!("name must not be empty");
        }

        if tokens.is_empty() {
            panic!("tokens must not be empty");
        }

        let module = Module {
            path: Path::new(DEFAULT_MODULE_PATH).to_path_buf(),
            flags: DEFAULT_MODULE_FLAGS,
            session_cache_size: DEFAULT_SESSION_CACHE_SIZE,
            initialize: false,
            tokens,
            name,
        };

        return module;
    }

    pub fn set_path(&mut self, path: &Path) {
        if path.to_str().unwrap().is_empty() {
            panic!("path is mut not be empty");
        }

        self.path = path.to_path_buf();
    }

    pub fn set_flags(&mut self, flags: i32) {
        self.flags = flags;
    }

    pub fn set_session_cache_size(&mut self, size: usize) {
        self.session_cache_size = size;
    }

    pub fn set_nitialize(&mut self, initialize: bool) {
        self.initialize = initialize;
    }
}

#[derive(Debug, Clone, Copy)]
pub enum LogLevel {
    Disable = 0,
    Alert = 1,
    Critical = 2,
    Error = 3,
    Warning = 4, // default
    Notice = 5,
    Info = 6,
    Debug = 7,
}

impl fmt::Display for LogLevel {
    fn fmt(&self, formatter: &mut fmt::Formatter<'_>) -> fmt::Result {
        write!(formatter, "{}", *self as usize)
    }
}

#[derive(Debug)]
pub struct Options {
    modules: Vec<Module>,
    log_level: LogLevel,
    session_cache_size: usize,
}

impl Options {
    pub fn new(modules: Vec<Module>) -> Options {
        if modules.is_empty() {
            panic!("modules must not be empty");
        }

        let options = Options {
            log_level: DEFAULT_LOG_LEVEL,
            session_cache_size: DEFAULT_SESSION_CACHE_SIZE,
            modules,
        };

        return options;
    }

    pub fn set_log_level(&mut self, log_level: LogLevel) {
        self.log_level = log_level;
    }
}

#[allow(non_camel_case_types)]
pub enum ParamSet {
    Test,
    CryptoPro_A,
    CryptoPro_B,
    CryptoPro_C,
    CryptoPro_XchA,
    CryptoPro_XchB,
    TC26_256_A,
    TC26_256_B,
    TC26_256_C,
    TC26_256_D,
    TC26_512_Test,
    TC26_512_A,
    TC26_512_B,
    TC26_512_C,
}

impl ParamSet {
    fn to_nid(&self) -> nid::Nid {
        match self {
            Self::Test => Nid::ID_GOSTR3410_2001_TEST_PARAMSET,
            Self::CryptoPro_A => {
                Nid::ID_GOSTR3410_2001_CRYPTOPRO_A_PARAMSET
            }
            Self::CryptoPro_B => {
                Nid::ID_GOSTR3410_2001_CRYPTOPRO_B_PARAMSET
            }
            Self::CryptoPro_C => {
                Nid::ID_GOSTR3410_2001_CRYPTOPRO_C_PARAMSET
            }
            Self::CryptoPro_XchA => {
                Nid::ID_GOSTR3410_2001_CRYPTOPRO_XCHA_PARAMSET
            }
            Self::CryptoPro_XchB => {
                Nid::ID_GOSTR3410_2001_CRYPTOPRO_XCHB_PARAMSET
            }
            Self::TC26_256_A => Nid::ID_TC26_GOST_3410_2012_256_A_PARAMSET,
            Self::TC26_256_B => Nid::ID_TC26_GOST_3410_2012_256_B_PARAMSET,
            Self::TC26_256_C => Nid::ID_TC26_GOST_3410_2012_256_C_PARAMSET,
            Self::TC26_256_D => Nid::ID_TC26_GOST_3410_2012_256_D_PARAMSET,
            Self::TC26_512_Test => {
                Nid::ID_TC26_GOST_3410_2012_512_TEST_PARAMSET
            }
            Self::TC26_512_A => Nid::ID_TC26_GOST_3410_2012_512_A_PARAMSET,
            Self::TC26_512_B => Nid::ID_TC26_GOST_3410_2012_512_B_PARAMSET,
            Self::TC26_512_C => Nid::ID_TC26_GOST_3410_2012_512_C_PARAMSET,
        }
    }
}

pub enum KeyPairAlgorithm {
    GOST3410_2012_256,
    GOST3410_2012_512,
}

impl KeyPairAlgorithm {
    fn to_id(&self) -> Id {
        match self {
            Self::GOST3410_2012_256 => Id::GOST3410_2012_256,
            Self::GOST3410_2012_512 => Id::GOST3410_2012_512,
        }
    }
}

pub struct KeyGenOptions {
    pub algorithm: KeyPairAlgorithm,
    pub param_set: ParamSet,
    pub label: String,
    pub ephemeral: bool,
    pub extractable: bool,
    pub sensitive: bool,
    pub token: Option<Token>,
    // pub module_name: Option<String>,
}

pub struct Engine {
    engine: openssl::engine::Engine,
    options: Options,
}

impl Engine {
    pub fn new(options: Options) -> Result<Engine> {
        openssl::init();

        let engine = openssl::engine::Engine::by_id(ENGINE_ID)?;
        engine.init()?;
        engine.set_default(openssl::engine::EngineMethod::ALL)?;

        let gost_engine = Engine { engine, options };

        return Ok(gost_engine);
    }

    pub fn init(&self) -> Result<()> {
        self.set_log_level(self.options.log_level)?;

        for module in &self.options.modules {
            self.init_module(module)?;
        }

        return Ok(());
    }

    pub fn init_module(&self, module: &Module) -> Result<()> {
        let init_arg = format!(
            "pkcs11:?module-name={}&module-path={}&itp11-flags={}&itp11-cache-size={}",
            module.name,
            module.path.to_str().unwrap(),
            module.flags,
            module.session_cache_size
        );

        self.engine
            .ctrl_cmd_string(ITP11_INIT_PKCS11, Some(&init_arg), 0)?;

        for token in &module.tokens {
            self.open_token(&module.name, token)?;
        }

        return Ok(());
    }

    pub fn open_token(&self, module_name: &str, token: &Token) -> Result<()> {
        let mut open_arg = format!("pkcs11:slot-id={}?module-name={}", token.slot, module_name);

        if !token.pin.is_empty() {
            open_arg += &format!("&pin-value={}", token.pin);
        }

        if token.set_default {
            open_arg += "&itp11-default=yes";
        }

        // TODO: handle AlreadyExistsErrorCode
        self.engine
            .ctrl_cmd_string("ITP11_OPEN_TOKEN", Some(&open_arg), 0)
    }

    pub fn finalize(&self) -> Result<()> {
        self.engine.ctrl_cmd_string(FINAL_CMD, None, 0)
    }

    pub fn load_private_key_by_label(&self, label: &str) -> Result<PKey<Private>> {
        let key_id = format!("pkcs11:object={};type=private", encode_label(label));
        self.engine.load_private_key(&key_id)
    }

    pub fn load_private_key_by_id(&self, id: &[u8]) -> Result<PKey<Private>> {
        let key_id = format!("pkcs11:id={};type=private", encode_id(id));
        self.engine.load_private_key(&key_id)
    }

    pub fn load_public_key_by_label(&self, label: &str) -> Result<PKey<Public>> {
        let key_id = format!("pkcs11:object={};type=public", encode_label(label));
        self.engine.load_public_key(&key_id)
    }

    pub fn load_public_key_by_id(&self, id: &[u8]) -> Result<PKey<Public>> {
        let key_id = format!("pkcs11:id={};type=public", encode_id(id));
        self.engine.load_public_key(&key_id)
    }

    pub fn set_log_level(&self, log_level: LogLevel) -> Result<()> {
        self.engine
            .ctrl_cmd_string(SET_LOG_LEVEL_CMD, Some(&log_level.to_string()), 0)
    }

    pub fn generate_private_key(&self, options: KeyGenOptions) -> Result<PKey<Private>> {
        let ctx = PKeyCtx::new(options.algorithm.to_id())?;
        ctx.keygen_init()?;

        if options.token.is_some() {
            let token = options.token.unwrap();

            let mut uri = String::new();
            write!(uri, "pkcs11:slot-id={}?pin-value={}", token.slot, token.pin).unwrap();

            ctx.ctrl_str(ITP11_EVP_PKEY_CTRL_STRING_TOKEN_URI, &uri)?;
        }

        ctx.ctrl(
            Nid::from_raw(ITP11_EVP_PKEY_CTRL_SET_GOST_PARAMSET),
            options.param_set.to_nid(),
        )?;

        if !options.label.is_empty() {
            ctx.ctrl_str(ITP11_EVP_PKEY_CTRL_STRING_CKA_LABEL, &options.label)?;
        }

        ctx.ctrl_str(
            ITP11_EVP_PKEY_CTRL_STRING_CKA_TOKEN,
            &(!options.ephemeral).to_string(),
        )?;

        ctx.ctrl_str(
            ITP11_EVP_PKEY_CTRL_STRING_CKA_EXTRACTABLE,
            &options.extractable.to_string(),
        )?;            

        ctx.ctrl_str(
            ITP11_EVP_PKEY_CTRL_STRING_CKA_SENSITIVE,
            &options.sensitive.to_string(),
        )?;

        ctx.keygen()
    }
}

impl Drop for Engine {
    fn drop(&mut self) {
        self.finalize().unwrap();
    }
}

fn encode_id(data: &[u8]) -> String {
    let string = hex::encode(data);

    let capacity = string.len() as f64 * 1.5;
    let mut encoded = String::with_capacity(capacity as usize);

    for (i, ch) in string.chars().enumerate() {
        if i & 1 == 0 {
            encoded.push('%');
        }

        encoded.push(ch);
    }

    return encoded;
}

fn encode_label(label: &str) -> String {
    label.replace(" ", "%20")
}

#[cfg(test)]
mod tests {
    use super::*;

    #[test]
    fn test_encode_label() {
        let label = "My key label";
        let expected = "My%20key%20label";

        let encoded = encode_label(label);
        assert_eq!(expected, encoded);
    }

    #[test]
    fn test_encode_id() {
        let expected = "%06%cc%dd%07%b6%d9%6d%0b%1c%8d%b4%4f%88%65%af%8d%af%86%8c%7b";

        let id = [
            0x06, 0xcc, 0xdd, 0x07, 0xb6, 0xd9, 0x6d, 0x0b, 0x1c, 0x8d, 0xb4, 0x4f, 0x88, 0x65,
            0xaf, 0x8d, 0xaf, 0x86, 0x8c, 0x7b,
        ];

        let encoded = encode_id(&id);
        assert_eq!(expected, encoded);
    }
}
