use std::fs;
use std::path::Path;
use std::mem;
use std::sync::{Arc, Mutex, Once};

use openssl::hash::{hash, MessageDigest};
use openssl::nid::Nid;
use openssl::pkey::{PKey, Private};
use openssl::x509::{X509StoreContext, X509, X509Ref};
use openssl::x509::crl::{X509CRL, X509CRLRef};
use openssl::x509::store::X509StoreBuilder;
use openssl::x509::verify::X509VerifyFlags;

use maplit::hashmap;

use openssl_itp11gost_engine::*;

const TEST_SLOT: usize = 30;
const TEST_PIN: &str = "00000000";
const TEST_MODULE_NAME: &str = "test_module";
const TEST_PRIVATE_KEY_LABEL: &str = "/tls/keystore/server.key.pem";
const TEST_PRIVATE_KEY_ID: &str = "06ccdd07b6d96d0b1c8db44f8865af8daf868c7b";
const TEST_PUBLIC_KEY_ID: &str = "06ccdd07b6d96d0b1c8db44f8865af8daf868c7b";

#[derive(Clone)]
struct SingletonReader {
    // Since we will be used in many threads, we need to protect
    // concurrent access
    inner: Arc<Mutex<Engine>>,
}

fn singleton() -> SingletonReader {
    // Initialize it to a null value
    static mut SINGLETON: *const SingletonReader = 0 as *const SingletonReader;
    static ONCE: Once = Once::new();

    unsafe {
        ONCE.call_once(|| {
            // Make it
            let engine_result = init_engine();
            if let Err(error) = engine_result {
                panic!("Невозможно инициализировать движок: {:?}", error);
            }

            let engine = engine_result.unwrap();

            let reader = SingletonReader {
                inner: Arc::new(Mutex::new(engine)),
            };

            // Put it in the heap so it can outlive this call
            let reader_ref = Box::new(reader);

            SINGLETON = mem::transmute(reader_ref);
        });

        // Now we give out a copy of the data that is safe to use concurrently.
        (*SINGLETON).clone()
    }
}

fn init_engine() -> Result<Engine> {
    let mut token = Token::new(TEST_SLOT, TEST_PIN.to_string());
    token.set_default(true);
    let tokens = vec![token];

    let module = Module::new(TEST_MODULE_NAME.to_string(), tokens);

    let modules = vec![module];

    let mut options = Options::new(modules);
    options.set_log_level(LogLevel::Debug);

    let engine = Engine::new(options)?;
    engine.init()?;

    return Ok(engine);
}

#[test]
fn test_engine_load_private_key_by_label() {
    let reader = singleton();
    let engine = reader.inner.lock().unwrap();

    let key_result = engine.load_private_key_by_label(TEST_PRIVATE_KEY_LABEL);
    assert!(key_result.is_ok());

    // let private_key = key_result.unwrap();
    // println!("{:?}", private_key);
}

#[test]
fn test_engine_load_private_key_by_id() {
    let id = hex::decode(TEST_PRIVATE_KEY_ID).unwrap();

    let reader = singleton();
    let engine = reader.inner.lock().unwrap();

    let key_result = engine.load_private_key_by_id(&id);
    assert!(key_result.is_ok());
}

#[test]
fn test_engine_load_public_key_by_id() {
    let id = hex::decode(TEST_PUBLIC_KEY_ID).unwrap();

    let reader = singleton();
    let engine = reader.inner.lock().unwrap();

    let key_result = engine.load_public_key_by_id(&id);
    assert!(key_result.is_ok());
}

#[test]
fn test_read_private_key() {
    let reader = singleton();
    let _engine = reader.inner.lock().unwrap();

    let result = open_private_key("testdata/tls/keystore/server.key.pem");
    assert!(result.is_ok());
}

#[test]
fn test_sign_verify() {
    let reader = singleton();
    let _engine = reader.inner.lock().unwrap();

    let message_digest = MessageDigest::from_nid(Nid::ID_GOSTR3411_2012_256)
        .expect("Создать компонент подсчёта контрольной суммы");

    let key_data =
        fs::read("testdata/tls/keystore/server.key.pem").expect("Прочитать закрытый ключ");

    let private_key =
        PKey::private_key_from_pem(&key_data).expect("Создать экземпляр закрытого ключа");

    let mut signer = openssl::sign::Signer::new(message_digest, &private_key)
        .expect("Создать компонент подписи");

    let message = hex::decode("06ccdd07b6d96d0b1c8db44f8865af8daf868c7b").unwrap();

    let signature = signer
        .sign_oneshot_to_vec(&message)
        .expect("Создать подпись");

    let mut verifier = openssl::sign::Verifier::new(message_digest, &private_key)
        .expect("Создать компонент проверки подписи");
    let valid = verifier
        .verify_oneshot(&signature, &message)
        .expect("Проверить подпись");

    assert!(valid);
}

#[test]
fn test_hash() {
    let message = "github.com/hyperledger/fabric";

    let test_data = hashmap![
        Nid::ID_GOSTR3411_2012_256 => "9557b55c9cf5fe3f3dab8c2dffd1510eb4bd3943782b77646874ce6fd362bbef",
        Nid::ID_GOSTR3411_2012_512 => "f0d9a2ba871996f0cf6a189a551dddeff876d0290198632a5cd24800705df1bbb85c3c7107489d191c67c81bac995507d25d214b12692596117998176f2c88ad",
    ];

    let reader = singleton();
    let _engine = reader.inner.lock().unwrap();

    for (nid, expected_digest) in test_data {
        let message_digest =
            MessageDigest::from_nid(nid).expect("Создать компонент подсчёта контрольной суммы");

        let expected = hex::decode(expected_digest).unwrap();

        let digest = hash(message_digest, message.as_bytes()).expect("Создать хэш");
        assert_eq!(&expected, digest.as_ref());
    }
}

#[test]
fn test_generate_private_key() {
    let reader = singleton();
    let engine = reader.inner.lock().unwrap();

    let options = KeyGenOptions {
        algorithm: KeyPairAlgorithm::GOST3410_2012_256,
        param_set: ParamSet::Test,
        label: "test_key_pair".to_string(),
        ephemeral: false,
        extractable: false,
        sensitive: true,
        token: None,
    };

    let result = engine.generate_private_key(options);
    assert!(result.is_ok(), "Не удалось сгенерировать ключ: {:?}", result.err());

    // let private_key = result.unwrap();
    // println!("Сгенерирован {:?}", private_key);

    // let priv_key_data = private_key.private_key_to_pem_pkcs8().unwrap();
    // let pub_key_data = private_key.public_key_to_pem().unwrap();

    // println!("{:?}", String::from_utf8(priv_key_data).unwrap());
    // println!("{:?}", String::from_utf8(pub_key_data).unwrap());
}

pub fn verify_certificate_chain(ca_certificate: &X509Ref, certificate: &X509Ref, crl: &X509CRLRef) -> std::result::Result<bool, Box<dyn std::error::Error>> {
    let flags = X509VerifyFlags::CRL_CHECK | X509VerifyFlags::CRL_CHECK_ALL;

    let mut store_builder = X509StoreBuilder::new()?;
    store_builder.add_cert(ca_certificate)?;
    store_builder.add_crl(crl)?;
    store_builder.set_flags(flags)?;
    let store = store_builder.build();

    // let mut param = X509VerifyParam::new()?;
    // let now = SystemTime::now();
    // let check_time = now.checked_add(Duration::from_secs(20*365*24*60*60)).unwrap();
    // param.set_time(check_time);

    let mut context = X509StoreContext::new()?;
    context.init(&store, certificate, None)?;
    // context.set_param(param);
    context.verify_cert().map_err(|err| err.into())
}

#[test]
fn test_verify_cert() {
    let reader = singleton();
    let _engine = reader.inner.lock().unwrap();

    let ca_certificate = open_certificate("testdata/tls/cacerts/ca.cert.pem")
                            .expect("Открыть CA сертификат");

    let certificate = open_certificate("testdata/tls/certs/server.cert.pem")
                            .expect("Открыть сертификат");

    let empty_crl = open_crl("testdata/tls/crls/empty.crl.pem")
                            .expect("Открыть список отозванных сертификатов");

    let mut valid = verify_certificate_chain(&ca_certificate, &certificate, &empty_crl)
                    .expect("Проверить цепочку сертификатов");
    assert!(valid);

    let revoked_crl = open_crl("testdata/tls/crls/revoked.crl.pem")
                            .expect("Открыть список отозванных сертификатов");

    valid = verify_certificate_chain(&ca_certificate, &certificate, &revoked_crl)
                    .expect("Проверить цепочку сертификатов");
    assert!(!valid);
}

pub fn open_private_key<P: AsRef<Path>>(path: P) -> std::result::Result<PKey<Private>, Box<dyn std::error::Error>> {
    let data = fs::read(path)?;
    PKey::private_key_from_pem(&data).map_err(|err| err.into())
}

pub fn open_certificate<P: AsRef<Path>>(path: P) -> std::result::Result<X509, Box<dyn std::error::Error>> {
    let data = fs::read(path)?;
    X509::from_pem(&data).map_err(|err| err.into())
}

pub fn open_crl<P: AsRef<Path>>(path: P) -> std::result::Result<X509CRL, Box<dyn std::error::Error>> {
    let data = fs::read(path)?;
    X509CRL::from_pem(&data).map_err(|err| err.into())
}
